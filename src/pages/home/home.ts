import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';

import { LocationModalPage } from '../modal/location-modal/location-modal';
import { PropertyModalPage } from '../modal/property-modal/property-modal';
import { RangeModalPage } from '../modal/range-modal/range-modal';
import { SelectModalPage } from '../modal/select-modal/select-modal';
import { ListPage } from '../list/list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  selectedLocations: Array < { id: number, title: string } > ;
  selectedProperties: Array < { name: string } > ;
  selectedPrice: any = {};
  selectedValue: any = {};
  locationBoolVal: boolean = false;
  propertyBoolVal: boolean = false;
  priceBoolVal: boolean = false;
  bedroomBoolVal: boolean = false;
  bathroomBoolVal: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    console.log(navCtrl);
  }

  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  showLocationModal() {
    let locationModal = this.modalCtrl.create(LocationModalPage, {}, {
      showBackdrop: false
    });
    locationModal.onDidDismiss(data => {
      if ((!this.isEmpty(data) && this.locationBoolVal) || (!this.isEmpty(data) && !this.locationBoolVal)) {
        this.selectedLocations = data;
        this.locationBoolVal = true;
      } else if (this.isEmpty(data) && !this.locationBoolVal) {
        this.locationBoolVal = false;
      }
    });
    locationModal.present();
  }

  showPropertyModal() {
    let propertyModal = this.modalCtrl.create(PropertyModalPage, {}, {
      showBackdrop: false
    });
    propertyModal.onDidDismiss(data => {
      if ((!this.isEmpty(data) && this.propertyBoolVal) || (!this.isEmpty(data) && !this.propertyBoolVal)) {
        this.selectedProperties = data;
        this.propertyBoolVal = true;
      } else if (this.isEmpty(data) && !this.propertyBoolVal) {
        this.propertyBoolVal = false;
      }
    });
    propertyModal.present();
  }

  showRangeModal(characterNum) {
    let rangeModal = this.modalCtrl.create(RangeModalPage, {id: characterNum}, {
      showBackdrop: false
    });
    rangeModal.onDidDismiss(data => {
      if ((!this.isEmpty(data) && this.priceBoolVal) || (!this.isEmpty(data) && !this.priceBoolVal)) {
        this.selectedPrice = data;
        this.priceBoolVal = true;
      } else if (this.isEmpty(data) && !this.priceBoolVal) {
        this.priceBoolVal = false;
      }
    });
    rangeModal.present();
  }

  showSelectModal(characterNum) {
    let select = this.modalCtrl.create(SelectModalPage, {id: characterNum}, {
      showBackdrop: false
    });
    select.onDidDismiss(data => {
      if ((!this.isEmpty(data) && this.bedroomBoolVal && !this.bathroomBoolVal) || (!this.isEmpty(data) && !this.bedroomBoolVal && this.bathroomBoolVal)) {
        this.selectedValue = data;
        this.bedroomBoolVal = true;
        this.bathroomBoolVal = false;
      } else if (this.isEmpty(data) && !this.bedroomBoolVal && this.bathroomBoolVal) {
        this.bedroomBoolVal = false;
        this.bathroomBoolVal = true;
      }
    });
    select.present();
  }

    openPage() {
      // Reset the content nav to have just this page
      // we wouldn't want the back button to show in this scenario
      this.navCtrl.setRoot(ListPage, {}, {animate: true});
  }

}
