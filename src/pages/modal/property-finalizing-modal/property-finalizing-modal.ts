import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the PropertyFinalizingModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-property-finalizing-modal',
  templateUrl: 'property-finalizing-modal.html',
})
export class PropertyFinalizingModalPage {

  currentChoice: any = [];

  data = {
      name: 'Property Finalizing',
      list: [
            {
              "name": "Not Finalizing",
              "active": false
            }, 
            {
              "name": "Normal Finalizing",
              "active": false
            },
            {
              "name": "Super Finalizing",
              "active": false
            },
            {
              "name": "High Class Finalizing",
              "active": false
            }
      ]
    }

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PropertyFinalizingModalPage');
  }

  getPropertyValue(item){
    item.active = !item.active;
    this.currentChoice.push(item.name);
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
