import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertyFinalizingModalPage } from './property-finalizing-modal';

@NgModule({
  declarations: [
    PropertyFinalizingModalPage,
  ],
  imports: [
    IonicPageModule.forChild(PropertyFinalizingModalPage),
  ],
})
export class PropertyFinalizingModalPageModule {}
