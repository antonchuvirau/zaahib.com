import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the PropertyModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-property-modal',
  templateUrl: 'property-modal.html',
})
export class PropertyModalPage {

  propertyData:any;
  currentChoice: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

   let propertyDataList = [
      {
        name: 'Province',
        items: [
          {
            list: [
              {
                "name": "Al Bahah",
                "active": false
              }, 
              {
                "name": "Al Jowf",
                "active": false
              },
              {
                "name": "Al Madinah",
                "active": false
              },
              {
                "name": "Al Qassim",
                "active": false
              },
              {
                "name": "Riyadh",
                "active": false
              },
              {
                "name": "Aseer",
                "active": false
              },
              {
                "name": "Eastern Province",
                "active": false
              },
              {
                "name": "Hail",
                "active": false
              },
              {
                "name": "Jazan",
                "active": false
              },
              {
                "name": "Makkah",
                "active": false
              },
              {
                "name": "Najran",
                "active": false
              },
              {
                "name": "Northern Borders",
                "active": false
              },
              {
                "name": "Tabuk",
                "active": false
              },
              {
                "name": "Property Outside Saudi Arabia",
                "active": false
              }
            ]
          }
        ]
      },
      {
        name: 'Property Type',
        items: [
          {
            title: 'Resedential',
            list: [
              {
                "name": "Appartment",
                "active": false
              }, 
              {
                "name": "Single Room",
                "active": false
              },
              {
                "name": "Villa",
                "active": false
              } 
            ]
          },
          {
            title: 'Entertainment',
            list: [
              {
                "name": "Chalet",
                "active": false
              }
            ]
          },
          {
            title: 'Commercial', 
            list: [
              {
                "name": "Office",
                "active": false
              },
              {
                "name": "Land",
                "active": false
              },
              {
                "name": "Raw Land",
                "active": false
              }
            ] 
          }
        ]
      },
      {
        name: 'Features',
        items: [
          {
            list: [
              {
                "name": "Electricity",
                "active": false
              }, 
              {
                "name": "Water",
                "active": false
              },
              {
                "name": "Gas",
                "active": false
              },
              {
                "name": "Phone",
                "active": false
              },
              {
                "name": "Internet",
                "active": false
              },
              {
                "name": "Furnished",
                "active": false
              },
              {
                "name": "Basement",
                "active": false
              },
              {
                "name": "Swimming Pool",
                "active": false
              },
              {
                "name": "Modern Design",
                "active": false
              },
              {
                "name": "AC",
                "active": false
              },
              {
                "name": "Central AC",
                "active": false
              },
              {
                "name": "Full Kitchen",
                "active": false
              },
              {
                "name": "Jacuzzi",
                "active": false
              },
              {
                "name": "Disability Features",
                "active": false
              },
              {
                "name": "External Extension",
                "active": false
              },
              {
                "name": "Room for Maid",
                "active": false
              },
              {
                "name": "Room for Driver",
                "active": false
              },
              {
                "name": "Storage Room",
                "active": false
              },
              {
                "name": "Security Gaurds",
                "active": false
              },
              {
                "name": "Security Cameras",
                "active": false
              },
              {
                "name": "Security Room",
                "active": false
              },
              {
                "name": "Fireplace",
                "active": false
              },
              {
                "name": "Residential",
                "active": false
              },
              {
                "name": "Commercial",
                "active": false
              },
              {
                "name": "Workers Residence",
                "active": false
              },
              {
                "name": "Singles Friendly",
                "active": false
              },
              {
                "name": "City View",
                "active": false
              },
              {
                "name": "Sea View",
                "active": false
              },
              {
                "name": "Waterfront",
                "active": false
              },
              {
                "name": "Garden",
                "active": false
              },
              {
                "name": "Elevator",
                "active": false
              },
              {
                "name": "Open Space Style",
                "active": false
              },
              {
                "name": "Hall",
                "active": false
              },
              {
                "name": "New Build, Not Used",
                "active": false
              }
            ]
          }
        ]
      }
    ]

    this.propertyData = propertyDataList[this.navParams.get('id')];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PropertyModalPage');
  }

  getPropertyValue(item){
    item.active = !item.active;
    this.currentChoice.push(item.name);
  }

  dismiss(){
    this.viewCtrl.dismiss(this.currentChoice);
  }


}
