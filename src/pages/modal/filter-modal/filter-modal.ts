import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { FilterLocationModalPage } from '../filter-location-modal/filter-location-modal';
import { PropertyModalPage } from '../property-modal/property-modal';
import { RangeModalPage } from '../range-modal/range-modal';
import { SelectModalPage } from '../select-modal/select-modal';
import { PropertyFinalizingModalPage } from '../property-finalizing-modal/property-finalizing-modal';
import { DirectoryPage } from '../../directory/directory';


/**
 * Generated class for the FilterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter-modal',
  templateUrl: 'filter-modal.html',
})
export class FilterModalPage { 

  constructor(public navCtrl: NavController, public appCtrl: App, public navParams: NavParams, public viewCtrl: ViewController, public modalCtrl: ModalController) {
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterModalPage');
  }

  showLocationModal() {
    let locationModal = this.modalCtrl.create(FilterLocationModalPage, {}, {
      showBackdrop: false
    });
    locationModal.onDidDismiss(data => {
    });
    locationModal.present();
  }

  showPropertyModal(modalId) {
    let propertyModal = this.modalCtrl.create(PropertyModalPage, { id:modalId }, {
      showBackdrop: false
    });
    propertyModal.onDidDismiss(data => {
    });
    propertyModal.present();
  }

  showRangeModal(modalId) {
    let priceModal = this.modalCtrl.create(RangeModalPage, {id: modalId}, {
      showBackdrop: false
    });
    priceModal.onDidDismiss(data => {

    });
    priceModal.present();
  }

  showSelectModal(modalId) {
    let select = this.modalCtrl.create(SelectModalPage, {id: modalId}, {
      showBackdrop: false
    });
    select.onDidDismiss(data => {
      
    });
    select.present();
  }

  showPropertyFModal() {
    let propertyF = this.modalCtrl.create(PropertyFinalizingModalPage, {}, {
      showBackdrop: false
    });
    propertyF.onDidDismiss(data => {
      
    });
    propertyF.present();
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  openPage(){
    this.appCtrl.getRootNav().setRoot(DirectoryPage, {}, {animate: true});
    this.dismiss();
  }

}
