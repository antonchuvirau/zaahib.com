import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { MapPage } from '../../map/map';

/**
 * Generated class for the LocationModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location-modal',
  templateUrl: 'location-modal.html',
})
export class LocationModalPage {
  
  currentLocation: any = [];
  searchResults: Array<{id: number, title: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

    this.searchResults = [
      { id: 1, title: 'Riyadh'},
      { id: 2, title: 'Jeddah'},
      { id: 3, title: 'Mecca'},
      { id: 4, title: 'Dammam'},
      { id: 5, title: 'Jubail'},
      { id: 6, title: 'Khobar'},
      { id: 7, title: 'Abha' }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationModalPage');
  }

  getItem(a){
    this.currentLocation.push(a);
  }

  dismiss(){
    this.viewCtrl.dismiss(this.currentLocation);
  }

  push() {
    this.navCtrl.pop();
    this.navCtrl.push(MapPage);
  }

}
