import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the RangeModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-range-modal',
  templateUrl: 'range-modal.html',
})
export class RangeModalPage {

  rangeModalData;
  rangeData: any = {};

  getValue(ev: any) {
    this.rangeData.min = ev._valA;
    this.rangeData.max = ev._valB;
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

    var rangesModalData = [
      {
        name: 'Price',
        model: 'price',
        data: {
          lower: 1,
          upper: 2000000
        },
        min: 1,
        max: 2000000,
        prefix: 'SAR'
      },
      {
        name: 'Area',
        model: 'area',
        data: {
          lower: 1,
          upper: 300
        },
        min: 1,
        max: 300,
        prefix: 'm2'
      }
    ];
    
    this.rangeModalData = rangesModalData[this.navParams.get('id')];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RangeModalPage');
  }

  dismiss(a, b){
    this.viewCtrl.dismiss(this.rangeData);
  }

}
