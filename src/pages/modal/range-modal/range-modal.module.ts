import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RangeModalPage } from './range-modal';

@NgModule({
  declarations: [
    RangeModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RangeModalPage),
  ],
})
export class RangeModalPageModule {}
