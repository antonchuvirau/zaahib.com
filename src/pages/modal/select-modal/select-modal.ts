import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the SelectModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-modal',
  templateUrl: 'select-modal.html',
})
export class SelectModalPage {

  selectModalData;
  
  selectedValues: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

    let selectsModalData = [
      {
        id: 1,
        name: 'Bedroom',
        model: {
          min: 'bedroomMin',
          max: 'bedroomMax'
        }
      },
      {
        id: 2,
        name: 'Bathroom',
        model: {
          min: 'bathroomMin',
          max: 'bathroomMax'
        }
      },
      {
        id: 3,
        name: 'Property Age',
        model: {
          min: 'ageMin',
          max: 'ageMax'
        }
      },
      {
        id: 4,
        name: 'Garage size',
        model: {
          min: 'sizeMin',
          max: 'sizeMax'
        }
      },
      {
        id: 5,
        name: 'Street binding',
        model: {
          min: 'bindingMin',
          max: 'bindingMax'
        }
      }
    ];
    
    this.selectModalData = selectsModalData[this.navParams.get('id')];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectModalPage');
  }

  dismiss(){
    this.viewCtrl.dismiss(this.selectedValues);
  }

  getMinValue(ev: any){
    this.selectedValues.min = ev._value.minute;
  }

  getMaxValue(ev: any){
    this.selectedValues.max = ev._value.minute;
  }


}
