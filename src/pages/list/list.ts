import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { TabMapPage } from '../tab-map/tab-map';
import { DirectoryPage } from '../directory/directory';
import { FilterModalPage } from '../modal/filter-modal/filter-modal';
import { SortModalPage } from '../modal/sort-modal/sort-modal';
import { DetailPage } from '../detail/detail';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {

  filters:Number;

  items = [
    {
      images: [
        { url: '../../assets/imgs/slide-img.jpg' },
        { url: '../../assets/imgs/slide-img.jpg' },
        { url: '../../assets/imgs/slide-img.jpg' },
        { url: '../../assets/imgs/slide-img.jpg' }
      ],
      type: 'Apartment',
      note: 'Sale',
      name: 'Damac Properties Co. LLC',
      bedRooms: 2,
      bathRooms: 3,
      area: 140,
      price: '1,228,080'
    },
    {
      images: [
        { url: '../../assets/imgs/slide-img.jpg' },
        { url: '../../assets/imgs/slide-img.jpg' },
        { url: '../../assets/imgs/slide-img.jpg' },
        { url: '../../assets/imgs/slide-img.jpg' }
      ],
      type: 'Apartment',
      note: 'Sale',
      name: 'Damac Properties Co. LLC',
      bedRooms: 2,
      bathRooms: 3,
      area: 140,
      price: '1,228,080'
    }
  ]

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.filters = 3;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
  }

  showFilterModal() {
    let filter = this.modalCtrl.create(FilterModalPage, { }, {
      showBackdrop: false
    });
    filter.onDidDismiss(data => {

    });
    filter.present();
  }

  showSortModal() {
    let sortModal = this.modalCtrl.create(SortModalPage, {}, {
      showBackdrop: false
    });
    sortModal.onDidDismiss(data => {
    });
    sortModal.present();
  }

  openDirectoryPage(){
    this.navCtrl.setRoot(DirectoryPage, {}, {animate: true});
  }

  openMapPage(){
    this.navCtrl.setRoot(TabMapPage, {}, {animate: true});
  }

  pushPage(){
    this.navCtrl.push(DetailPage);
  }
}
