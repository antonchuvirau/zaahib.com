import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

import { FeaturesPage } from '../features/features';
import { ContactsDetailPage } from '../contacts-detail/contacts-detail';
import { MapDetailPage } from '../map-detail/map-detail';

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {

  @ViewChild('sliderOne') sliderOne: Slides;
  @ViewChild('sliderTwo') sliderTwo: Slides;
  @ViewChild('sliderThree') sliderThree: Slides;

  carousel = {
      type: 'Apartment',
      note: 'Sale',
      name: 'Damac Properties Co. LLC',
      imagesUrl: [
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'}
      ],
      bedrooms: 2,
      bathrooms: 4,
      area: 140,
      price: '1,228,080'
    }
  
  similarData = [
    {
      type: 'Apartment',
      note: 'Sale',
      name: 'Damac Properties Co. LLC',
      imagesUrl: [
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'}
      ],
      bedrooms: 2,
      bathrooms: 4,
      area: 140,
      price: '1,228,080'
    },
    {
      type: 'Apartment',
      note: 'Sale',
      name: 'Damac Properties Co. LLC',
      imagesUrl: [
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'}
      ],
      bedrooms: 2,
      bathrooms: 4,
      area: 140,
      price: '1,228,080'
    },
    {
      type: 'Apartment',
      note: 'Sale',
      name: 'Damac Properties Co. LLC',
      imagesUrl: [
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'}
      ],
      bedrooms: 2,
      bathrooms: 4,
      area: 140,
      price: '1,228,080'
    },
    {
      type: 'Apartment',
      note: 'Sale',
      name: 'Damac Properties Co. LLC',
      imagesUrl: [
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'},
        { url: '../../assets/imgs/detail-img.jpg'}
      ],
      bedrooms: 2,
      bathrooms: 4,
      area: 140,
      price: '1,228,080'
    }
  ];

  icons = {
    urls: [
      { url: '../../assets/icon/electicity.svg' },
      { url: '../../assets/icon/water.svg' },
      { url: '../../assets/icon/phone.svg' },
      { url: '../../assets/icon/internet.svg' },
      { url: '../../assets/icon/swimming-pool.svg' },
      { url: '../../assets/icon/full-kitchen.svg' }
    ],
    iconsNumber: 12
  }

  pages = [ContactsDetailPage, MapDetailPage, FeaturesPage,];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }

  goBack(){
    this.navCtrl.pop();
  }



  pushPage(a){
    let index = a;
    this.navCtrl.push(this.pages[index]);
  }
}
