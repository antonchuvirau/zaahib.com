import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ListPageModule } from '../pages/list/list.module';
import { DirectoryPage } from '../pages/directory/directory';
import { DirectoryPageModule } from '../pages/directory/directory.module';
import { TabMapPage } from '../pages/tab-map/tab-map';
import { TabMapPageModule } from '../pages/tab-map/tab-map.module';
import { DetailPage } from '../pages/detail/detail';
import { DetailPageModule } from '../pages/detail/detail.module';
import { FeaturesPageModule } from '../pages/features/features.module';
import { ContactsDetailPageModule } from '../pages/contacts-detail/contacts-detail.module';
import { MapDetailPageModule } from '../pages/map-detail/map-detail.module';

//Modals
import { LocationModalPage } from '../pages/modal/location-modal/location-modal';
import { LocationModalPageModule } from '../pages/modal/location-modal/location-modal.module';
import { PropertyModalPage } from '../pages/modal/property-modal/property-modal';
import { PropertyModalPageModule } from '../pages/modal/property-modal/property-modal.module';
import { RangeModalPage } from '../pages/modal/range-modal/range-modal';
import { RangeModalPageModule } from '../pages/modal/range-modal/range-modal.module';
import { SelectModalPage } from '../pages/modal/select-modal/select-modal';
import { SelectModalPageModule } from '../pages/modal/select-modal/select-modal.module';
import { SortModalPage } from '../pages/modal/sort-modal/sort-modal';
import { SortModalPageModule } from '../pages/modal/sort-modal/sort-modal.module';
import { FilterModalPage } from '../pages/modal/filter-modal/filter-modal';
import { FeaturesPage } from '../pages/features/features';
import { ContactsDetailPage } from '../pages/contacts-detail/contacts-detail';
import { MapDetailPage } from '../pages/map-detail/map-detail';
import { FilterModalPageModule } from '../pages/modal/filter-modal/filter-modal.module';
import { FilterLocationModalPage } from '../pages/modal/filter-location-modal/filter-location-modal';
import { FilterLocationModalPageModule } from '../pages/modal/filter-location-modal/filter-location-modal.module';
import { PropertyFinalizingModalPage } from '../pages/modal/property-finalizing-modal/property-finalizing-modal';
import { PropertyFinalizingModalPageModule } from '../pages/modal/property-finalizing-modal/property-finalizing-modal.module';
//Map
import { MapPage } from '../pages/map/map';
import { MapPageModule } from '../pages/map/map.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    DirectoryPageModule,
    ListPageModule,
    PropertyModalPageModule,
    RangeModalPageModule,
    FilterModalPageModule,
    LocationModalPageModule,
    TabMapPageModule,
    SortModalPageModule,
    SelectModalPageModule,
    FilterLocationModalPageModule,
    MapPageModule,
    FeaturesPageModule,
    DetailPageModule,
    ContactsDetailPageModule,
    PropertyFinalizingModalPageModule,
    MapDetailPageModule,
    IonicModule.forRoot(MyApp, {menuType: 'overlay'}, {
      links: [
        { component: HomePage, segment: 'home' },
        { component: ListPage, segment: 'list', defaultHistory: [ListPage] },
        { component: DirectoryPage, segment: 'directory', defaultHistory: [DirectoryPage] },
        { component: TabMapPage, segment: 'map-location', defaultHistory: [TabMapPage] },
        { component: DetailPage, segment: 'detail', defaultHistory: [DetailPage] },
        { component: FeaturesPage, segment: 'features', defaultHistory: [FeaturesPage] },
        { component: ContactsDetailPage, segment: 'contacts-detail', defaultHistory: [ContactsDetailPage] },
        { component: MapDetailPage, segment: 'map-detail', defaultHistory: [MapDetailPage] }
      ]
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LocationModalPage,
    PropertyModalPage,
    RangeModalPage,
    SelectModalPage,
    MapPage,
    FilterModalPage,
    FilterLocationModalPage,
    TabMapPage,
    SortModalPage,
    DetailPage,
    FeaturesPage,
    ContactsDetailPage,
    MapDetailPage,
    PropertyFinalizingModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
